-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings, FlexibleInstances, NamedFieldPuns #-}

module GameInitModel where

import Position
import Data.Aeson
import Control.Applicative ((<$>), (<*>))

-- Dimension

data Dimension = Dimension {
  dimensionLength   :: Double,
  width             :: Double,
  guideFlagPosition :: Double
} deriving (Show)

instance FromJSON Dimension where
  parseJSON (Object v) =
    Dimension <$>
    (v .: "length") <*>
    (v .: "width") <*>
    (v .: "guideFlagPosition")

-- CarId

data CarId = CarId {
  color     :: String,
  carIdName :: String
} deriving (Show)

instance FromJSON CarId where
  parseJSON (Object v) =
    CarId <$>
    (v .: "color") <*>
    (v .: "name")

-- Car

data Car = Car {
  carId      :: CarId,
  dimensions :: Dimension
} deriving (Show)

instance FromJSON Car where
  parseJSON (Object v) =
    Car <$>
    (v .: "id") <*>
    (v .: "dimensions")

-- Lane

data Lane = Lane {
  distanceFromCenter :: Double,
  index              :: Int
} deriving (Show)

instance FromJSON Lane where
  parseJSON (Object v) =
    Lane <$>
    (v .: "distanceFromCenter") <*>
    (v .: "index")

-- Piece

data Piece =
  Straight {
    startPoint :: StartingPoint,
    isSwitch :: Bool,
    isBridge :: Bool,
    pieceLength :: Double
  } |
  Bend {
    startPoint :: StartingPoint,
    isSwitch :: Bool,
    radius :: Double,
    pieceAngle :: Double
  }
  deriving (Show, Eq, Ord)

instance FromJSON Piece where
  parseJSON (Object v) = do
    isSw <- v .:? "switch" .!= False
    len <- v .:? "length"
    case len of
      Just f -> do
        isBr <- v .:? "bridge" .!= False
        return $ Straight dummyStartingPoint isSw isBr f
      Nothing -> do
        r <- v .: "radius"
        a <- v .: "angle"
        return $ Bend dummyStartingPoint isSw r a

-- StartingPoint

data StartingPoint = StartingPoint {
  position :: Position,
  angle    :: Double
} deriving (Show, Eq, Ord)

dummyStartingPoint = StartingPoint (Position nan nan) nan
  where nan = 0.0 / 0.0

instance FromJSON StartingPoint where
  parseJSON (Object v) =
    StartingPoint <$>
    (v .: "position") <*>
    (v .: "angle")

-- Track

data Track = Track {
  name          :: String,
  startingPoint :: StartingPoint,
  pieces        :: [Piece],
  lanes         :: [Lane]
} deriving (Show)

instance FromJSON Track where
  parseJSON (Object v) =
    Track <$>
    (v .: "name") <*>
    (v .: "startingPoint") <*>
    (v .: "pieces") <*>
    (v .: "lanes")

-- RaceSession

data RaceSession = RaceSession {
  laps :: Maybe Int,
  durationMs :: Maybe Int,
  maxLapTimeMs :: Maybe Int,
  quickRace :: Maybe Bool
} deriving (Show)

instance FromJSON RaceSession where
  parseJSON (Object v) =
    RaceSession <$>
    (v .:? "laps") <*>
    (v .:? "durationMs") <*>
    (v .:? "maxLapTimeMs") <*>
    (v .:? "quickRace")

data GameInitData = GameInitData {
  track :: Track,
  cars  :: [Car],
  raceSession :: RaceSession
} deriving (Show)

instance FromJSON GameInitData where
  parseJSON (Object v) = do
    race <- v .: "race"
    GameInitData <$>
      (race .: "track") <*>
      (race .: "cars") <*>
      (race .: "raceSession")

-- Helpers

players :: GameInitData -> [CarId]
players gameInit =
  map (\car -> carId $ car) $ cars gameInit

piecesOfGame :: GameInitData -> [Piece]
piecesOfGame gameInit =
  pieces $ track gameInit

startingPointOfGame :: GameInitData -> StartingPoint
startingPointOfGame gameInit =
  let (StartingPoint pos a) = startingPoint $ track gameInit
  in StartingPoint pos (a - 90) -- renderer is offset 90 degrees

lanesOfGame :: GameInitData -> [Lane]
lanesOfGame gameInit =
  lanes $ track gameInit

reportGameInit :: GameInitData -> String
reportGameInit gameInit =
  "Players: " ++ (show $ players gameInit) ++ ", Track: " ++ show (length $ piecesOfGame gameInit) ++ " pieces"
