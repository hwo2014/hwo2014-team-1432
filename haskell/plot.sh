#!/bin/bash
cd $(dirname "$0")
csvFields=$(cat AI.hs | grep 'in printCsv (csvHandle aiState)' | sed -e 's/.*in printCsv (csvHandle aiState) //' | tr '[],' '   ')

serverFields="serverPosX serverPosY serverAngle serverAngleOffset serverVelX serverVelY"
declare -A fieldMap
i=1
for field in $csvFields $serverFields; do
    [ "$1" = "-f" ] && echo $field $i
    fieldMap[$field]=$i
    ((i++))
done

[ "$1" = "-f" ] && exit

field() {
    if [ "${fieldMap[$1]}" = "" ]; then
        echo "Unknown field: $1" 1>&2
        exit 1
    fi
    echo "${fieldMap[$1]}"
}

dataFile=${1:-races/latest.dat}

rm -rf temp/*
cp $dataFile temp/data.dat

plotIndex=0
doPlot() {
    title="$1"
    shift

    xAxis="$1"
    shift

    fieldIndexes=""
    for f in $@; do
        fieldIndexes="\"temp/data.dat\" using $(field $xAxis):$(field $f) title \"$f\" with linespoints;$fieldIndexes"
    done

    columns=$(echo $fieldIndexes | sed -e 's/;/, /g' | sed -e 's/, $//')
    cat >> "temp/$plotIndex.gp" <<EOF
plot $columns;
EOF
    gnuplot -e 'set term pngcairo enhanced font "arial,10" size 1200,700' -e "set output \"temp/$plotIndex.png\"" "temp/$plotIndex.gp"
    (( plotIndex++))
}

doPlot "position" gameTick f g
doPlot "velocity" gameTick velocity acceleration
# doPlot "local vs server position" gameTick positionX positionY serverPosX serverPosY
#doPlot "local vs server velocity x" gameTick velocityX serverVelX
#doPlot "local vs server velocity y" gameTick velocityY serverVelY
doPlot "angles" gameTick slipAngle trackAngle

gpicview temp/*.png
