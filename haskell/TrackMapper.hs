-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings, FlexibleInstances, NamedFieldPuns #-}

module TrackMapper(angMod, getAbsolutePosition, getAbsolutePositionAndDirection, getTravelledDistance, evaluateBend, calculatePieceStartingPoints, getBendCentreOfCurvature) where

import Data.Fixed

import Position
import CarPositionsModel
import GameInitModel

angMod x = mod' x 360

evaluateBend bend@(Bend { radius, pieceAngle, startPoint = (StartingPoint pos angle) }) laneDelta amount =
  (getBendCentreOfCurvature bend |+| vecFromCenter, signum pieceAngle .* rotationVectorDerivative totalAngle)
  where fixup = if pieceAngle < 0 then 90 else -90
        totalAngle = pieceAngle * amount + angle + fixup
        vecFromCenter = (radius - laneDelta) .* rotationVector totalAngle

getAbsolutePosition game ppos = fst $ getAbsolutePositionAndDirection game ppos

getAbsolutePositionAndDirection game (PiecePosition { pieceIndex, inPieceDistance, lane = CarLane l _ }) =
  let laneDelta = distanceFromCenter $ lanesOfGame game !! l
  in case piecesOfGame game !! pieceIndex of
    Straight { startPoint = StartingPoint p a } ->
      (p |+| laneDelta .* normalVector rot |+| inPieceDistance .* rot, rot)
        where rot = rotationVector a
    bend@Bend { startPoint = StartingPoint p a, radius, pieceAngle } ->
      evaluateBend bend delta (inPieceDistance / (2 * pi * (radius - delta) * (abs pieceAngle / 360)))
        where delta = if angMod pieceAngle > 179.9 then -laneDelta else laneDelta

-- XXX: this is a bit problematic since lane changes will probably have a different length
getTravelledDistance game (PiecePosition { pieceIndex, inPieceDistance, lane = CarLane l _ }) =
  helper (piecesOfGame game) 0 0.0
  where
    helper (p:_) i dist | i == pieceIndex = dist + inPieceDistance
    helper ((Straight { pieceLength }):ps)   i dist = helper ps (i + 1) (dist + pieceLength)
    helper ((Bend { radius, pieceAngle }):ps) i dist = helper ps (i + 1) (dist + arcLength)
      where arcLength = (abs pieceAngle / 360) * (2 * pi * (radius - signum pieceAngle * laneDelta))
            laneDelta = distanceFromCenter $ lanesOfGame game !! l

getBendCentreOfCurvature (Bend { radius, pieceAngle, startPoint = (StartingPoint pos angle) })  =
  pos |+| radius .* (signum pieceAngle .* normalVector (rotationVector angle))

calculatePieceStartingPoints :: [Piece] -> StartingPoint -> [Piece]
calculatePieceStartingPoints [] _ = []
calculatePieceStartingPoints (p:ps) curStartPoint = newPoint : rest
  where
    newPoint = p { startPoint = curStartPoint }
    rest = calculatePieceStartingPoints ps (nextPoint newPoint curStartPoint)
    nextPoint (Straight { pieceLength }) (StartingPoint pos angle) =
      StartingPoint  (pos |+| pieceLength .* rotationVector angle) angle

    nextPoint (bend@Bend { radius, pieceAngle }) (StartingPoint pos angle) =
      StartingPoint {
        position = fst $ evaluateBend bend 0.0 1.0,
        angle = angMod $ angle + pieceAngle
      }
