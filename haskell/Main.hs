-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (stdin, stdout, stderr)

import Data.IORef
import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import System.IO.Unsafe
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))

import ServerMessage
import GameInitModel
import CarPositionsModel
import AI

type ClientMessage = String

-- Write line to stderr
putErrLn = hPutStrLn stderr

-- Write output to server socket
outputMessage h line = do
  hPutStrLn h line
  putStrLn $ "> " ++ line

joinMessage botname botkey Nothing = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
joinMessage botname botkey (Just track) = "{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"},\"trackName\":\"" ++ track ++ "\",\"carCount\":1}}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey Nothing
    [server, port, botname, botkey, track] -> do
      run server port botname botkey (Just track)
    _ -> do
      putErrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [<track>]"
      exitFailure

run server port botname botkey track = do
  h <- connectToServer server port
  hSetBuffering stdout LineBuffering
  hSetBuffering h LineBuffering
  outputMessage h $ joinMessage botname botkey track
  handleMessages h

handleMessages h = do
  msg <- hGetLine h
  putStrLn $ "< " ++ msg
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)


handleServerMessage :: Handle -> ServerMessage -> IO ()
handleServerMessage h serverMessage = do
  responses <- respond serverMessage
  forM_ responses $ outputMessage h
  handleMessages h

-- car name and GameInitData come in separate packets, so store car name here for a while
carNameRef = unsafePerformIO $ newIORef "<unset car name>"
aiStateRef = unsafePerformIO $ newIORef undefined :: IORef AiState

respond :: ServerMessage -> IO [ClientMessage]
respond message = case message of
  Join -> do
    putErrLn "Joined"
    return [pingMessage]
  YourCar carId -> do
    writeIORef carNameRef $ carIdName carId
    return [pingMessage]
  GameInit gameId gameInit -> do
    putErrLn $ "GameInit: " ++ (reportGameInit gameInit)
    carName <- readIORef carNameRef
    ai <- initAi gameId carName gameInit
    writeIORef aiStateRef ai
    return [pingMessage]
  CarPositions gameTick carPositions -> do
    ai <- readIORef aiStateRef
    (newState, msgs) <- handleCarPositions ai gameTick carPositions
    writeIORef aiStateRef newState
    return msgs
  Spawn -> do
    ai <- readIORef aiStateRef
    newState <- handleSpawn ai
    writeIORef aiStateRef newState
    return [pingMessage]
  Crash -> do
    ai <- readIORef aiStateRef
    newState <- handleCrash ai
    writeIORef aiStateRef newState
    return [pingMessage]
  Finish -> do
    return [pingMessage]
  Unknown msgType -> do
    putErrLn $ "Unknown message: " ++ msgType
    return [pingMessage]
