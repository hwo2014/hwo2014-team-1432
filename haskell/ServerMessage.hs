-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings, FlexibleInstances, NamedFieldPuns #-}
module ServerMessage where

import Data.Maybe
import Control.Monad
import Control.Applicative
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), (.!=), Result(..))

import Position
import TrackMapper
import GameInitModel
import CarPositionsModel

data ServerMessage =
  Join |
  YourCar CarId |
  GameInit String GameInitData |
  CarPositions Int [CarPosition] |
  Crash |
  Spawn |
  Finish |
  Unknown String

decodeMessage :: (String, Value, Maybe String, Int) -> Result ServerMessage
decodeMessage (msgType, msgData, gameId, gameTick)
  | msgType == "join" = Success Join
  | msgType == "yourCar" = YourCar <$> fromJSON msgData
  | msgType == "gameInit" = do
      gameData <- fromJSON msgData
      let trk@(Track{ startingPoint = StartingPoint pos a }) = track $ gameData
      let newPieces = calculatePieceStartingPoints (pieces trk) (StartingPoint (normalVector pos) (a - 90))
      return $ GameInit (fromJust gameId) $ gameData { track = trk { pieces = newPieces } }
  | msgType == "carPositions" = CarPositions gameTick <$> fromJSON msgData
  | msgType == "crash" = Success $ Crash
  | msgType == "spawn" = Success $ Spawn
  | msgType == "finish" = Success $ Finish
  | otherwise = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a, Maybe String, Int) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    gameId  <- v .:? "gameId"
    gameTick <- v .:? "gameTick" .!= 0
    return (msgType, msgData, gameId, gameTick)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
