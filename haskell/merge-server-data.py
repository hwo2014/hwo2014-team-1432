import urllib2
import gzip
import json
import os
import sys
from StringIO import StringIO

csvFile = open(sys.argv[1])
filePart = os.path.basename(os.path.realpath(sys.argv[1]))

line = csvFile.readline()
gameId = line.split()[1]

strio = StringIO(urllib2.urlopen('https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/' + gameId + '.json').read())
jsonData = gzip.GzipFile(fileobj=strio).read()
arr = json.loads(jsonData)

outFile = open('races/' + filePart, 'w')
curData = None
for obj in arr:
    tick = obj.get('gameTick')
    if not tick:
        continue

    data = obj['data']
    if obj['msgType'] == 'fullCarPositions':
        myCar = data[0] # FIXME
        curData = (myCar['coordinates']['x'], myCar['coordinates']['y'], myCar['angle'], myCar['angleOffset'])

    elif obj['msgType'] == 'carVelocities':
        myCar = data[0] # FIXME
        curData += (myCar['x'], myCar['y'])

        line = csvFile.readline().strip()
        if not line:
            break
        csvTick = line.split()[0]
        assert int(csvTick) == tick
        outFile.write(line + ' ' + ' '.join(map(str, curData)) + "\n")
        curData = None

try:
    os.unlink('races/latest.dat')
except OSError:
    pass
os.symlink(filePart, 'races/latest.dat')
