-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings, FlexibleInstances, NamedFieldPuns #-}
module Position where
import Data.Aeson
import Control.Applicative ((<$>), (<*>))

data Position = Position {
  x :: Double,
  y :: Double
} deriving (Show, Eq, Ord)

instance FromJSON Position where
  parseJSON (Object v) =
    Position <$>
    (v .: "x") <*>
    (v .: "y")

(|+|) :: Position -> Position -> Position
Position a b |+| Position c d = Position (a + c) (b + d)
(|-|) :: Position -> Position -> Position
Position a b |-| Position c d = Position (a - c) (b - d)

(.*) :: Double -> Position -> Position
k .* Position a b = Position (k * a) (k * b)

normalVector :: Position -> Position
normalVector (Position a b) = Position (-b) (a)

rad2deg x = x * 180 / pi
deg2rad x = x / 180 * pi
-- Clockwise = positive, in degrees
rotationVector angle = Position (cos (- deg2rad angle)) (-sin (- deg2rad angle))
rotationVectorDerivative angle = normalize $ Position (sin (- deg2rad angle)) (cos (- deg2rad angle))

vectorLength (Position x y) = sqrt $ x*x + y*y

normalize v = (1.0 / vectorLength v) .* v

infixl 6 |+|
infixl 6 |-|
infixl 7 .*
