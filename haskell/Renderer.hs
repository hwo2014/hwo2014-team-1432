-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE OverloadedStrings, FlexibleInstances, NamedFieldPuns #-}

module Renderer where

import Position
import Tracks
import TrackMapper
import CarPositionsModel
import GameInitModel
import ServerMessage

import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Display
import Graphics.Gloss.Interface.IO.Animate
import GHC.Float
import qualified Geom2D as G2
import Geom2D.CubicBezier(CubicBezier(..), evalBezier)
import Data.Fixed
import Control.Concurrent.MVar
import Control.Concurrent

consecutives :: [x] -> [(x,x)]
consecutives list = list `zip` (tail list)

f2d = float2Double
d2f = double2Float
dp2fp (x, y) = (d2f x, d2f y)

rad x = x * pi / 180
toFloatVector (Position x y) = (d2f x, d2f y)

thickArc' startAngle arc r t
  | arc < 0 = thickArc' (startAngle + arc + 180) (-arc) r t
  | otherwise = helper (angMod $ 270 + d2f startAngle) (angMod $ 270 + (d2f $ startAngle + arc))
    where helper a1 a2 | a2 < a1 && a2 <= 0.0 = thickArc a1 (a2 + 360) r t
                       | a2 < a1 && a2 > 0.0 = Pictures [thickArc a2 0 r t, thickArc 360 a1 r t]
                       | otherwise = thickArc a1 a2 r t

rect' w h = Polygon [(0, -h/2), (w, -h/2), (w, h/2), (0, h/2)]

thickness = 2
line' = line'' thickness
line'' t segs = Pictures $ map (convertSeg t) $ consecutives segs

convertSeg t (p1, p2) = polygon [
    toFloatVector $ p1 |+| r,
    toFloatVector $ p2 |+| r,
    toFloatVector $ p2 |+| s,
    toFloatVector $ p1 |+| s]
      where
        n = normalize $ normalVector (p2 |-| p1)
        r = (t / 2) .* n
        s = (-1.0) .* r

drawBezier bezier = line' $ map (\(G2.Point x y) -> Position x y) $ map (evalBezier bezier) [0, 0.05 .. 1]

quadBezier p0 p1 p2 = CubicBezier p0
                                  (p0 G2.^+^ (2/3) G2.*^ (p1 G2.^-^ p0))
                                  (p2 G2.^+^ (2/3) G2.*^ (p1 G2.^-^ p2))
                                  p2

trackWidth initData = 2 * (20 + (abs $ distanceFromCenter $ head $ lanesOfGame initData))

drawStraightLanes len lanes isSwitch = straightLanes ++ changeLanes
  where
    straightLanes = map (\(Lane dist _) -> line' [Position 0 dist, Position len dist]) lanes
    changeLanes = if not isSwitch then [] else map drawChangeLane $ consecutives lanes ++ consecutives (reverse lanes)
    drawChangeLane ((Lane d1 _), (Lane d2 _)) =
      let bezier = CubicBezier (G2.Point 0 d1) (G2.Point (len/2) d1)
                               (G2.Point (len/2) d2) (G2.Point len d2)
      in drawBezier bezier

drawTrack width lanes points = Color (greyN 0.5) $ Pictures $ map visualize points
    where
        visualize (Straight { startPoint = StartingPoint (Position x y) a, pieceLength, isSwitch }) =
          Translate (d2f x) (d2f y) $
            Rotate (- d2f a) $
              Pictures [
                rect' (d2f pieceLength) (d2f width),
                Color yellow $
                  Pictures $ drawStraightLanes pieceLength lanes isSwitch]

        visualize (bend@Bend { startPoint = (StartingPoint pos a), radius, pieceAngle, isSwitch }) =
          Pictures [
            Translate (fst center) (snd center) $
              Pictures [
                thickArc' a pieceAngle (d2f radius) (d2f width),
                Color yellow $
                  Pictures $
                    map (\(Lane dist _) -> normalLane dist) lanes],
            Color yellow $
              Pictures $
                if not isSwitch
                  then []
                  else map changeLane $ consecutives lanes ++ consecutives (reverse lanes)]
            where center = toFloatVector $ getBendCentreOfCurvature bend
                  normalLane dist = thickArc' a pieceAngle (d2f $ radius + dist) (d2f thickness)
                  changeLane ((Lane d1 _), (Lane d2 _)) = let
                          mirror = if pieceAngle > 0 then -1 else 1
                          (Position sX sY) = fst $ evaluateBend bend d1 0.0
                          (Position midX midY) = fst $ evaluateBend bend ((d1 + d2) / 2) 0.5
                          (Position endX endY) = fst $ evaluateBend bend d2 1.0
                          midpointX = ((d2 + d1) / 2 + radius) * cos(rad (a+pieceAngle)  / 2) * mirror
                          midpointY = ((d2 + d1) / 2 + radius) * sin(rad (a+pieceAngle)  / 2) * mirror
                          bezier = quadBezier (G2.Point sX sY)
                                              (G2.Point midX midY)
                                              (G2.Point endX endY)
                            in drawBezier bezier

inputThread gameMvar carsMvar = do
  msg <- getLine
  --putStrLn msg
  when (head msg == '<') $ do
    case decode (L.pack $ drop 2 msg) of
      Just json ->
        let decoded = fromJSON json >>= decodeMessage in
        case decoded of
          Success serverMessage -> case serverMessage of
            GameInit gameId gameInit -> do
              putMVar gameMvar gameInit
              return ()
            CarPositions gameTick carPositions -> do
              tryTakeMVar carsMvar
              putMVar carsMvar carPositions
              return ()
            Unknown msgType -> do
              putStrLn $ "Unknown message: " ++ msgType
            _ -> return ()
          Error s -> fail $ "Error decoding message: " ++ s
      Nothing -> do
        fail $ "Error parsing JSON: " ++ (show msg)
  inputThread gameMvar carsMvar

renderer gameMvar carsMvar _ = do
  game <- readMVar gameMvar
  positions <- readMVar carsMvar
  let ((Position x y), (Position dx dy)) = getAbsolutePositionAndDirection game (piecePosition $ head positions)
  let StartingPoint pos a = startingPointOfGame game
  let Dimension len width flagPos = dimensions $ head $ cars $ game
  let newPieces = calculatePieceStartingPoints (piecesOfGame game) (StartingPoint (normalVector pos) a)
  let trk = drawTrack (trackWidth game) (lanesOfGame game) newPieces
  let trackAngle = rad2deg $ atan2 dy dx
  let slipAngle = carAngle $ head positions

  return $ Scale 1 (-1) $ Pictures [
    trk,
    Translate (d2f x) (d2f y) $ Pictures [
      Color red $ Rotate (-(d2f $ trackAngle + slipAngle)) $ Pictures [
        line'' width [Position ((-len) + flagPos) 0, Position flagPos 0]],
      Color black $ line' [Position 0 0, 15 .* Position dx dy]]]

testMain = do
  let game = germany
  let StartingPoint pos a = startingPointOfGame game
  let newPieces = calculatePieceStartingPoints (piecesOfGame game) (StartingPoint (normalVector pos) a)
  let n = 170
  let trk@(Color _ (Pictures lst)) = drawTrack (trackWidth game) (lanesOfGame game) (take n newPieces)
  mapM_ print lst
  mapM_ print $ take n $ newPieces
  animateIO (InWindow "HWO" (300, 300) (0, 0)) black (\_ -> return $ Scale 1 (-1) $ trk)

realMain = do
  gameMvar <- newEmptyMVar
  carsMvar <- newEmptyMVar
  forkIO $ animateIO (InWindow "HWO" (1800, 950) (0, 0)) black (renderer gameMvar carsMvar)
  inputThread gameMvar carsMvar

main = realMain
