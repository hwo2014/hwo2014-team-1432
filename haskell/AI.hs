-- vim: set sw=2 ts=2 sts=2 expandtab:
{-# LANGUAGE ScopedTypeVariables, NamedFieldPuns #-}
module AI where

import Position
import GameInitModel
import TrackMapper
import CarPositionsModel

import Prelude hiding (catch)
import Control.Monad
import System.IO
import Control.Exception(catch)
import System.Posix.Files
import Data.IORef
import Data.Time
import System.Locale(defaultTimeLocale)

throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

data TickData = TickData {
  tickNum :: Int,
  tickPosition :: Double,
  tickAngle :: Double,
  tickThrottle :: Double
} deriving (Show)

data AiState = AiState {
  gameId :: String,
  csvFile :: String,
  csvHandle :: Handle,
  gameData :: GameInitData,
  carName :: String,
  currentPosition :: Position,
  currentVelocity :: Position,
  currentAcceleration :: Position,
  isCrashed :: Bool,
  tickDatas :: [TickData]
} deriving (Show)

initAi :: String -> String -> GameInitData -> IO AiState
initAi id cname game = do
  time <- getZonedTime
  let baseName = formatTime defaultTimeLocale "%Y-%m-%d_%H:%M:%S.dat" time
  let fileName = "unprocessed-races/" ++ baseName
  handle <- openFile fileName WriteMode
  -- symlink unprocessed-races/latest.dat to the latest race
  let latestFile = "unprocessed-races/latest.dat"
  catch (removeLink latestFile) (\(_ :: IOError) -> return ())
  createSymbolicLink baseName latestFile

  hSetBuffering handle LineBuffering
  hPutStrLn handle $ "# " ++ id ++ " " ++ name (track game)

  return $ AiState {
    gameId = id,
    csvFile = fileName,
    csvHandle = handle,
    gameData = game,
    carName = cname,
    currentPosition = Position 0 0, -- will be updated later...
    currentVelocity = Position 0 0,
    currentAcceleration = Position 0 0,
    isCrashed = False,
    tickDatas = []
  }

printCsv handle cols = hPutStrLn handle $ unwords cols

atan2' y x = let angle = rad2deg $ atan2 y x
             in if angle < 0 then angle + 360 else angle

getCurvature game (PiecePosition { pieceIndex, lane = CarLane l _ }) =
  let laneDelta = distanceFromCenter $ lanesOfGame game !! l
      piece = piecesOfGame game !! pieceIndex
  in case piece of
    Bend { radius, pieceAngle } -> (signum pieceAngle) / (radius - signum pieceAngle * laneDelta)
    _ -> 0

printStateToCsv aiState tick myCar throt = do
  let
    ((Position px py), (Position dx dy)) =
      getAbsolutePositionAndDirection (gameData aiState) (piecePosition myCar)
    gameTick = show tick
    travelledDistance = show $ getTravelledDistance (gameData aiState) (piecePosition myCar)
    positionX = show px
    positionY = show py
    velocityX = show $ x $ currentVelocity aiState
    velocityY = show $ y $ currentVelocity aiState
    accelerationX = show $ x $ currentAcceleration aiState
    accelerationY = show $ y $ currentAcceleration aiState
    acceleration  = show $ vectorLength $ currentAcceleration aiState
    velocity  = show $ vectorLength $ currentVelocity aiState
    throttle  = show throt
    slipAngle = show $ carAngle myCar
    trackAngle = show $ atan2' dy dx
    velocityAngle = show $ atan2' (y $ currentVelocity aiState) (x $ currentVelocity aiState)
    crashed   = if isCrashed aiState then "1" else "0"
    curvature = show $ getCurvature (gameData aiState) (piecePosition myCar)
    f = show $ maybe 0.0 fst3 $ calculateConstants $ tickDatas aiState
    g = show $ maybe 0.0 snd3 $ calculateConstants $ tickDatas aiState
    in printCsv (csvHandle aiState) [gameTick, travelledDistance, positionX, positionY, velocityX, velocityY, velocity, accelerationX, accelerationY, acceleration, throttle, slipAngle, trackAngle, velocityAngle, crashed, curvature, f, g]

fst3 (x, y, z) = x
snd3 (x, y, z) = y

updatePositionAndVelocity st@(AiState { currentPosition, currentVelocity }) myCar =
  let newPos = getAbsolutePosition (gameData st) (piecePosition myCar)
      newVel = newPos |-| currentPosition
      newAcc = newVel |-| currentVelocity
  in st { currentPosition = newPos, currentVelocity = newVel, currentAcceleration = newAcc }

handleCarPositions :: AiState -> Int -> [CarPosition] -> IO (AiState, [String])
handleCarPositions aiState gameTick carPositions = do
  let Just myCar = findCar (carName aiState) carPositions
  if gameTick == 0
    then do
      let newState = aiState {
          currentPosition = getAbsolutePosition (gameData aiState) (piecePosition myCar),
          currentVelocity = Position 0 0
      }
      return (newState, [pingMessage])
    else do
      if isCrashed aiState
        then do
          printStateToCsv aiState gameTick myCar 0.0
          return (aiState, [pingMessage])
        else do
          -- mapM_ (hPutStrLn stderr . show) $ tickDatas aiState
          let st' = if gameTick >= startTick then makeReport gameTick aiState myCar else aiState
          let newState = updatePositionAndVelocity st' myCar
          let throttle = if gameTick <= endTick then constThrottle else throttleFor myCar
          printStateToCsv aiState gameTick myCar throttle
          hPutStrLn stderr $ show $ calculateConstants $ tickDatas newState
          return (newState, [throttleMessage throttle])
  where makeReport gameTick st myCar = st {
          tickDatas = (TickData gameTick
                                (getTravelledDistance (gameData st) (piecePosition myCar))
                                (carAngle myCar)
                                constThrottle):(safeTail $ tickDatas st)

        }
        startTick = 5
        endTick = startTick + 1

safeTail xs = id $ take 40 $ id $ xs

calculateConstants xs | length xs < 20 = Nothing
                      | otherwise = Just (f, g, h)
  where
    (v:vels) = zipWith (\x y -> tickPosition x - tickPosition y) xs (tail xs)
    accs = zipWith (\x y -> x - y) (v:vels) vels

    (av:angVels) = zipWith (\x y -> tickAngle x - tickAngle y) xs (tail xs)
    angAccs = zipWith (\x y -> x - y) (av:angVels) angVels

    (a1:a2:_) = accs
    (v1:v2:_) = vels

    f = (a1 - a2) / (v2 - v1)
    g = (a1 * v2 - a2 * v1) / (constThrottle * (v2 - v1))

    n = length xs
    h = (42, 42)
    --a1 = vectorLength $ tickAcceleration d1
    --a2 = vectorLength $ tickAcceleration d2
    --v1 = vectorLength $ tickVelocity d1
    --v2 = vectorLength $ tickVelocity d2

constThrottle = 0.50 :: Double

handleCrash st = return $ st {
  isCrashed = True,
  currentVelocity = Position 0 0
}

handleSpawn st = return $ st { isCrashed = False }

throttleFor :: CarPosition -> Double
throttleFor myCar = if myAngle < 5
                      then constThrottle
                      else constThrottle
  where myAngle = abs $ carAngle myCar
